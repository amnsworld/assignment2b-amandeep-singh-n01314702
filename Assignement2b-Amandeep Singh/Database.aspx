﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Database.aspx.cs" Inherits="Assignement2b_Amandeep_Singh.Database" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TrickyConcept" runat="server">
    <h3>Tricky Concept in Database - GROUP BY clause</h3>
    <p>The GROUP BY clause is a SQL command that is used to group rows that have the same values.
        The GROUP BY clause is used in the SELECT statement .Optionally it is used in conjunction with aggregate functions to produce 
        summary reports from the database.</p>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CodeSnippet" runat="server">
    <h3>Query that gets a list of all the vendors with no invoices </h3>
    <p>
        <%--select ven.vendor_name<br />
        from vendors ven<br />
        full join invoices inv<br />
        on inv.vendor_id = ven.vendor_id<br />
        group by ven.vendor_name,inv.vendor_id<br />
        having COUNT(inv.invoice_id)=0;<br />--%>
        
        <usercontrol:codeBox ID="teacher_database_code" runat="server" SkinId="dataGridSkin" code="Database" owner="teacher"></usercontrol:codeBox>

    </p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ExampleSnippetCode" runat="server">
    <h3>This Query lists the number of customers in each country.</h3>
    <p>
        <%--SELECT COUNT(CustomerID), Country<br />
        FROM Customers<br />
        GROUP BY Country;</p>    --%>    
        
    
    <usercontrol:codeBox ID="my_database_code" runat="server" SkinId="dataGridSkin" code="Database" owner="me"></usercontrol:codeBox>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="HelpFullLinks" runat="server">
    <h3>Useful Resource links</h3>
    <a href="https://www.w3schools.com/sql/sql_groupby.asp">W3 Schools</a><br />
    <a href="https://www.techonthenet.com/sql/group_by.php">Tech On The Net</a><br />
    <a href="https://www.dofactory.com/sql/group-by">Do Factory</a><br />
</asp:Content>