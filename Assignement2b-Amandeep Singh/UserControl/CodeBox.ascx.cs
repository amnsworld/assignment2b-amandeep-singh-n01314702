﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignement2b_Amandeep_Singh.UserControl
{
    public partial class CodeBox : System.Web.UI.UserControl
    {
        public string code
        {
            get { return (string)ViewState["code"]; }
            set { ViewState["code"] = value; }
        }
        public string owner
        {
            get { return (string)ViewState["owner"]; }
            set { ViewState["owner"] = value; }
        }

        DataView CreateCode(List<string> codeList)
        {
            DataTable databaseCode = new DataTable();
            DataColumn ln_col = new DataColumn();
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            ln_col.ColumnName = " Line";
            ln_col.DataType = System.Type.GetType("System.Int32");
            databaseCode.Columns.Add(ln_col);

            code_col.ColumnName = " Code";
            code_col.DataType = System.Type.GetType("System.String");
            databaseCode.Columns.Add(code_col);


            int i = 0;
            foreach (string code_line in codeList)
            {
                coderow = databaseCode.NewRow();
                coderow[ln_col.ColumnName] = i;
                string final_code = System.Net.WebUtility.HtmlEncode(code_line);
                final_code = final_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = final_code;

                i++;
                databaseCode.Rows.Add(coderow);
            }

            DataView codeview = new DataView(databaseCode);
            return codeview;

        }

        protected void Page_Load(object sender, EventArgs e)
        {


            List<string> database_teacher_code = new List<string>(new string[]{
                "select ven.vendor_name",
                "from vendors ven",
                "full join invoices inv",
                "on inv.vendor_id = ven.vendor_id",
                "group by ven.vendor_name,inv.vendor_id",
                "having COUNT(inv.invoice_id)=0;"
            });
            List<string> database_my_code = new List<string>(new string[]{
                "SELECT COUNT(CustomerID), Country",
                "FROM Customers",
                "GROUP BY Country;"
            });


            List<string> javascript_teacher_code = new List<string>(new string[]{
                "var cars = [\"Audi\", \"Volvo\", \"BMW\"];"
            });
            List<string> javascript_my_code = new List<string>(new string[]{
                "var myTunes = [];",
                "myTunes.push(\"At the Hop\");",
                "myTunes.push(\"Penny Lane\");",
                "myTunes.push(\"Disco Inferno\");",
                "myTunes.push(\"The Reflex\");",
                "myTunes.push(\"Wonderwall\");",
                "console.log(myTunes);"
            });


            List<string> digitaldesign_teacher_code = new List<string>(new string[]{
             "<table border =\"1\">",
             "<thead>",
             "~<tr>",
             "~~<th> First Name </th>",
             "~~<th> Last Name </th>",
             "~</tr>",
             "</thead>",
             "<tbody>",
             "~<tr>",
             "~~<td> John </td>",
             "~~<td> Doe </td>",
             "~</tr>",
             "</tbody>",
             "</table>"
            });
            List<string> digitaldesign_my_code = new List<string>(new string[]{
             "<!DOCTYPE html>",
             "<html>",
             "<body>",
             "~<table border =\"1\">",
             "~<thead>",
             "~~<tr>",
             "~~~<th> First Name </th>",
             "~~~<th> Last Name </th>",
             "~~</tr>",
             "~</thead>",
             "~<tbody>",
             "~~<tr>",
             "~~~<td> John </td>",
             "~~~<td> Doe </td>",
             "~~</tr>",
             "~</tbody>",
             "~</table>",
             "</ body >",
             "</ html >"
            });

            List<string> display_code = new List<string>(new String[] { });


            if (code == "JavaScript" && owner == "teacher")
            {
                display_code = javascript_teacher_code;
            }
            else if (code == "JavaScript" && owner == "me")
            {
                display_code = javascript_my_code;
            }
            else if (code == "Database" && owner == "teacher")
            {
                display_code = database_teacher_code;
            }
            else if (code == "Database" && owner == "me")
            {
                display_code = database_my_code;
            }
            else if (code == "DigitalDesign" && owner == "teacher")
            {
                display_code = digitaldesign_teacher_code;
            }
            else if (code == "DigitalDesign" && owner == "me")
            {
                display_code = digitaldesign_my_code;
            }

            DataView dv = CreateCode(display_code);
            DataGrid_container.DataSource = dv;
            DataGrid_container.DataBind();

        }
    }
}