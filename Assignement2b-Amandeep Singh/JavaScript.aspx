﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="JavaScript.aspx.cs" Inherits="Assignement2b_Amandeep_Singh.JavaScript" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TrickyConcept" runat="server">
    <h3>Tricky Concept in Java Script - Arrays</h3>
    <p>
        The Array object lets you store multiple values in a single variable. 
        It stores a fixed-size sequential collection of elements of the same type. An array is used to store a collection of data, 
        but it is often more useful to think of an array as a collection of variables of the same type.
    </p>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CodeSnippet" runat="server">
    <h3>A variable cars is used to store different types of cars like Audi,Volvo,BMW etc...</h3>
    <p>
        <%--var cars = ["Audi", "Volvo", "BMW"];--%>

        <usercontrol:codeBox ID="teacher_javascript_code" runat="server" SkinId="dataGridSkin" code="JavaScript" owner="teacher"></usercontrol:codeBox>
    </p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ExampleSnippetCode" runat="server">
    <h3>This Query declares a variable myTunes and gets data using push property.Displays the values on Console</h3>
    <p>
        <%--var myTunes = [];<br />
        myTunes.push("At the Hop");<br />
        myTunes.push("Penny Lane");<br />
        myTunes.push("Disco Inferno");<br />
        myTunes.push("The Reflex");<br />
        myTunes.push("Wonderwall");<br />
        console.log(myTunes);<br />--%>

        <usercontrol:codeBox ID="my_javascript_code" runat="server" SkinId="dataGridSkin" code="JavaScript" owner="me"></usercontrol:codeBox>
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="HelpFullLinks" runat="server">
    <h3>Useful Resource links</h3>
    <a href="https://www.w3schools.com/js/js_arrays.asp">W3 Schools</a><br />
    <a href="https://www.tutorialspoint.com/javascript/javascript_arrays_object.htm">Tutorials Point</a><br />
</asp:Content>